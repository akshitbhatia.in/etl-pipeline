import os
import unittest
from load_hospital_users import LoadHospitalUsers, is_csv
from postgresql.create_table import Create_Table

ROOT_DIR = os.path.dirname(os.path.abspath(__name__))
file_path = os.path.join(ROOT_DIR, "data_file_20211212.csv")



class Test_hospital_user(unittest.TestCase):
    def setup(self):
        self.create_table = Create_Table()
        self.load_hospital_users = LoadHospitalUsers(file_path)
        self.raw_data = None
        self.columns = None


    def test_schema(self):
        self.setup()
        self.columns, self.raw_data = self.load_hospital_users.get_raw_data()
        data = self.load_hospital_users.extract_data(self.raw_data)
        for (Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State, Country, DOB, Is_Active) in data:
            print((Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State, Country, DOB, Is_Active))
            self.assertLess(len(Customer_Name), 255)
            self.assertLess(len(Customer_Id), 18)
            self.assertLess(len(Open_Date), 9)
            self.assertLess(len(Last_Consulted_Date), 9)
            self.assertLess(len(Vaccination_Id), 5)
            self.assertLess(len(Dr_Name), 255)
            self.assertLess(len(State), 5)
            self.assertLess(len(Country), 5)
            self.assertLess(len("".join("1987-03-06".split("-"))) , 9)
            self.assertLess(len(Is_Active), 8)


    def test_columns(self):
        self.setup()
        self.columns, self.raw_data = self.load_hospital_users.get_raw_data()
        print(self.columns[0], "|H|Customer_Name|Customer_Id|Open_Date|Last_Consulted_Date|Vaccination_Id|Dr_Name|State|Country|DOB|Is_Active")
        self.assertEqual(self.columns[0], "|H|Customer_Name|Customer_Id|Open_Date|Last_Consulted_Date|Vaccination_Id|Dr_Name|State|Country|DOB|Is_Active")


    def Test_iscsv(self):
        print(is_csv(file_path), True)
        self.assertTrue(is_csv(file_path), True)


if __name__ == '__main__':
    unittest.main()