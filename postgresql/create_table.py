import psycopg2
import os
import json
import logging
ROOT_DIR = os.path.dirname(__file__)


class Create_Table:
	def __init__(self):
		self.logger = logging.getLogger()
		self.logger.setLevel(logging.DEBUG)
		handler = logging.FileHandler(f"{os.path.abspath(__file__).replace('.py', '')}.log", 'w', 'utf-8')
		handler.setFormatter(logging.Formatter('%(name)s %(message)s'))
		self.logger.addHandler(handler)
		self.conn = self.get_connection()


	def create_hospital_table(self):
		""" create hospital tables in the PostgreSQL database(demo)"""
		command = (
			"""
			CREATE TABLE IF NOT EXISTs hospital_customers (
			Customer_Name VARCHAR(255) NOT NULL PRIMARY KEY,
			Customer_Id VARCHAR(18) NOT NULL,
			Open_Date DATE NOT NULL,
			Last_Consulted_Date DATE,
			Vaccination_Id CHAR(5),
			Dr_Name CHAR(255),
			State CHAR(5),
			Country CHAR(5),
			DOB DATE NOT NULL DEFAULT CURRENT_DATE,
			Is_Active CHAR(8)
			)
			"""
		)
		try:
			# read the connection parameters
			# connect to the PostgreSQL server
			cur = self.conn.cursor()
			# create table one by one
			cur.execute(command)

			# commit the changes
			self.conn.commit()
			self.logger.info(f"Hospital Table Created!!")
			print(f"\n{'*'*20} Hospital Table Created {'*'*20}\n")
		except (Exception, psycopg2.DatabaseError) as error:
			self.logger.exception(f"Table Error (hospital_customers): {error}")
			raise Exception(error)


	def get_config(self):
		""" Database auth Config """
		config_path = os.path.join(ROOT_DIR, "config.json")
		config_file = open(config_path, 'r')
		config = json.loads(config_file.read())
		config_file.close()
		return config


	def get_connection(self):
		self.logger.info(f"Postgres Connnection Created!")
		return psycopg2.connect(**self.get_config())


	def close(self):
		self.conn.close()
		self.logger.info(f"Close Postgres Connnection!")
