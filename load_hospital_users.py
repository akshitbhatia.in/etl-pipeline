import os
import csv
import pandas as pd
import argparse
from postgresql.create_table import Create_Table
import psycopg2
import traceback
import logging



class LoadHospitalUsers:
    def __init__(self, file_path):
        self.file_path = file_path
        self.Create_Table = Create_Table()
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler(f"{os.path.abspath(__file__).replace('.py', '')}.log", 'w', 'utf-8')
        handler.setFormatter(logging.Formatter('%(name)s %(message)s'))
        self.logger.addHandler(handler)


    def get_raw_data(self):
        """
        :return: Raw file data
        """
        with open(self.file_path) as data_file:
            csv_data = csv.reader(data_file)
            raw_data = [data for data in csv_data]
            columns = raw_data[0]
            raw_data = raw_data[1:]
            self.logger.info(f"Columns: {columns}")
            return columns, raw_data[1:]

    def extract_data(self, data):
        """
        :return:list
            Extracted Hospital User data
        """
        data = [(Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State,
                 Country, str(pd.to_datetime(DOB, format='%d%m%Y')).split(' ')[0], Is_Active) for (
                    id, H, Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State,
                    Country, DOB, Is_Active) in [row.split("|") for n_rows in data for row in n_rows]]
        self.logger.info(f"Extracted File Data count: {len(data)}")
        print(f"{'*'*20} File Data: {len(data)} {'*'*20}\n")
        return data


    def load_data(self, data):
        """
        :param data: File Data
        :return: Insert data to hospital table
        """
        try:
            for (Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State, Country, DOB, Is_Active) in data:
                self.Create_Table.conn.cursor().execute("INSERT INTO hospital_customers VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State, Country, DOB, Is_Active))
                self.logger.info(f"Insert to hospital table: {(Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State, Country, DOB, Is_Active)}")
                print((Customer_Name, Customer_Id, Open_Date, Last_Consulted_Date, Vaccination_Id, Dr_Name, State, Country, DOB, Is_Active))
                print()
            # commit the changes
            self.Create_Table.conn.commit()
            self.logger.info(f"Commited {len(data)} records!")

            # close communication with the PostgreSQL database server
            self.Create_Table.conn.close()
            print(f"\n {'*' * 20} {len(data)} Records inserted to hospital user table {'*' * 20}\n")
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.exception(f"Data insertion Error: {error}")
            raise Exception(error)


def is_csv(file_path):
    """

    :param file_path:
    :return: boolean
    Validation CSV file
    """
    is_file = os.path.isfile(file_path)
    if not is_file:
        print("\nPlease enter the valid file path(.csv)\n")
        return False

    file_extention = os.path.splitext(file_path)[1]
    if file_extention != ".csv":
        print("\nOnly `.csv` is valid!\n")
        return False
    return True



if __name__ == '__main__':
    # Get Args
    parser = argparse.ArgumentParser()
    parser.add_argument('--p', type=str, help="Please enter the valid json file path")
    args = parser.parse_args()
    file_path = str(args.p).strip()

    # Get File Path / File Path Validation
    if not is_csv(file_path):
        exit("\nPlease enter the valid file path(.csv)\n")

    try:
        load_hospital_users = LoadHospitalUsers(file_path)

        # Create Table
        load_hospital_users.Create_Table.create_hospital_table()

        # Get raw data
        columns, raw_data = load_hospital_users.get_raw_data()

        # Extract file data
        extracted_data = load_hospital_users.extract_data(raw_data)

        # Insert to the Staging table
        load_hospital_users.load_data(extracted_data)
        print(f"\nDone [+] {os.path.basename(__file__)}")
        load_hospital_users.logger.info(f"\nDone [+] {os.path.basename(__file__)}")
    except Exception as e:
        load_hospital_users.logger.exception(e)
        load_hospital_users.logger.error(f"\nFail [-] {os.path.basename(__file__)}")
        print(traceback.format_exc())
        print(f"\nFail [-] {os.path.basename(__file__)}")